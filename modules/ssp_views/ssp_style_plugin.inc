<?php


/**
  * Implementation of views_plugin_style().
  */
class ssp_style_plugin extends views_plugin_style {
  function option_definition() {
    //$options = parent::option_definition();
    $options['width'] = array('default' => '500');
    $options['height'] = array('default' => '400');
    $options['wmode'] = array('default' => 'opaque');
    $options['transitionStyle'] = array('default' => 'Blur');
    $options['path'] = array('default' => '');
    $options['transitionPause'] = array('default' => 2);
    $options['transitionLength'] = array('default' => 2);
    $options['bgcolor'] = array('default' => '#121212');
    $options['navThumbLinkSize'] = array('default' => '25,25');
    $options['navPosition'] = array('default' => 'Bottom');
    $options['navLinkAppearance'] = array('default' => 'Thumbnails');
    return $options;
  }
//'#options' => array('ie7' => t('IE7'), 'tango' => t('Tango'), 'custom' => t('Custom')),
  function options_form(&$form, &$form_state) {
   // parent::options_form($form, $form_state);
    $form['disclaimer'] = array(
      '#type' => 'markup',
      '#title' => t('Notes'),
      '#value' => '<strong>These settings will override the admin settings configured at admin/settings/ssp. If you leave the values blank, they should be overwritten by those settings.',
    );
    $form['width'] = array(
      '#type' => 'textfield',
      '#size' => 10,
      '#title' => t('Width'),
      '#default_value' => $this->options['width'],
    );
    $form['height'] = array(
      '#type' => 'textfield',
      '#title' => t('Height'),
      '#size' => 10,
      '#default_value' => $this->options['height'],
    );
    $form['path'] = array(
      '#type' => 'textfield',
      '#title' => t('Path'),
      '#description' => t('The absolute path to SlideShow Pro.'. ' <strong>Note: If this is blank it will read the value from admin/settings/ssp</strong>'),
      '#size' => 40,
      '#default_value' => $this->options['path'],
    );
    $form['wmode'] = array(
      '#type' => 'select',
      '#title' => t('Wmode'),
      '#options' => ssp_params_options('wmode'),
      '#default_value' => $this->options['wmode'],
      '#description' => t('SlideshowPro Player Wmode Options'),
    );
    $form['bgcolor'] = array(
        '#type' => 'textfield',
        '#title' => 'Background Color',
        '#size' => 9,
        '#default_value' => $this->options['bgcolor'],
        '#description' => t('SlideshowPro Player Background Color'),
    );
    $form['transitionStyle'] = array(
      '#type' => 'select',
      '#title' => t('Style'),
      '#description' => t('The transitioning style.'),
      '#options' => ssp_params_options('transitionStyle'),
      '#default_value' => $this->options['transitionStyle'],
    );
    $form['transitionLength'] = array(
        '#type'=>'textfield',
        '#title' => 'Length',
        '#description' => 'How long (in seconds) a slide is visible for',
        '#size' => 5,
        '#default_value' => $this->options['transitionLength'],
    );
    $form['transitionPause'] = array(
        '#type'=>'textfield',
        '#title' => 'Pause',
        '#description' => 'How long (in seconds) to pause before transitioning, ie the Load Indicator.',
        '#size' => 5,
        '#default_value' => $this->options['transitionPause'],
    );
    $form['navThumbLinkSize'] = array(
        '#type'=>'textfield',
        '#title' => 'Size',
        '#description' => 'Please insert in the format width, height <br>Example: 25,25 <br><strong>NOTE: You\'ll need to change the Imagecache settings so this image does not blur.</strong>',
        '#size' => 10,
        '#default_value' => $this->options['navThumbLinkSize'],
    );  
    $form['navPosition'] = array(
        '#type'=>'select',
        '#title' => 'Navigation Position',
        '#description' => 'Where the navigiation shows up',
        '#options' => ssp_params_options('navPosition'),
        '#default_value' => $this->options['navPosition'],
    ); 
    $form['navLinkAppearance'] = array(
        '#type'=>'select',
        '#title' => 'Navigdation Link Appearance',
        '#description' => '',
        '#options' => ssp_params_options('navLinkAppearance'),
        '#default_value' => $this->options['navLinkAppearance'],
    );
  } 
}
