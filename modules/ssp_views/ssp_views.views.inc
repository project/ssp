<?php

/**
  * Implementation of hook_views_plugin().
  */
function ssp_views_views_plugins() {
  return array(
    'module' => 'ssp_views',
    'style' => array(
      'ssp_views' => array(
        'title' => t('Slideshow Pro'),
        'theme' => 'ssp_views_view',
        'help' => t('Displays images in slideshowpro.'),
        'handler' => 'ssp_style_plugin',
        'uses row plugin' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal',
      ),
    ),
  );
}
